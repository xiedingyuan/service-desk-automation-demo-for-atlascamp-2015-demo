package com.atlassian.plugins.atlascamp.sdautomation.phonecaller;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.CallFactory;
import com.twilio.sdk.resource.instance.Call;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Implementation of {@link PhoneCaller} that uses Twilio to make phone calls.
 *
 */
@Component
public final class TwilioPhoneCaller implements PhoneCaller
{
    private static final Logger log = LoggerFactory.getLogger(TwilioPhoneCaller.class);

    // The Twilio SID, token and phone number listed here will not work as is.
    // You need to set up a Twilio account at https://www.twilio.com
    private static final String ACCOUNT_SID = "ACd79d77a5d0c137a6c0cdd0ede1a32c52";
    private static final String AUTH_TOKEN = "dce2428fa42adaff02b345aa0eb1eec6";
    private static final String TWILIO_PHONE_NUMBER = "+12562798783";
    private static final String TWIMLETS_URL = "http://twimlets.com/message";

    @Override
    public void call(final InternationalPhoneNumber recipient, final PhoneCallTranscript callContents)
    {
        log.debug("Making a phone call to {}", recipient);

        CallFactory callFactory = createCallFactory();
        makeCallUsingFactory(callFactory, recipient, callContents);
    }

    private CallFactory createCallFactory()
    {
        TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
        return client.getAccount().getCallFactory();
    }

    private void makeCallUsingFactory(
            final CallFactory callFactory,
            final InternationalPhoneNumber recipient,
            final PhoneCallTranscript callContents)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("To", recipient.getFullNumber()));
        params.add(new BasicNameValuePair("From", TWILIO_PHONE_NUMBER));
        params.add(new BasicNameValuePair("Url", toTwimletsUrl(callContents)));

        try
        {
            Call callMade = callFactory.create(params);
            log.debug("Call successfully made to {} with sid '{}'", recipient, callMade.getSid());
        }
        catch (TwilioRestException e)
        {
            log.error("Unable to make call to {} due to the following error: {}", recipient, e.getErrorMessage());
        }
    }

    private String toTwimletsUrl(final PhoneCallTranscript transcript)
    {

        final StringBuilder urlParams = new StringBuilder();
        int messageIndex = 0;
        for(final Iterator<String> lineIter = transcript.getLines().iterator(); lineIter.hasNext();)
        {
            urlParams.append("Message").append("%5B" + messageIndex++ + "%5D")
                    .append("=").append(urlEncode(lineIter.next()));

            if(lineIter.hasNext())
            {
                urlParams.append("&");
            }
        }

        return TWIMLETS_URL + "?" + urlParams.toString();
    }

    private String urlEncode(final String toEncode)
    {
        try
        {
            return URLEncoder.encode(toEncode, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            return toEncode;
        }
    }

}
