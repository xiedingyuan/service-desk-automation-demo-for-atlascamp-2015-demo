package com.atlassian.plugins.atlascamp.sdautomation;

import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.servicedesk.plugins.automation.api.configuration.ruleset.validation.ValidationResult;
import com.atlassian.servicedesk.plugins.automation.spi.rulethen.ThenActionValidator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;

import static com.atlassian.plugins.atlascamp.sdautomation.CallMeMaybe.PHONE_NUMBER_KEY;
import static com.atlassian.servicedesk.plugins.automation.api.configuration.ruleset.validation.ValidationResult.PASSED;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Responsible for checking that a phone number entered by the user is valid.
 *
 */
public final class PhoneNumberValidator implements ThenActionValidator
{
    private final I18nHelper.BeanFactory i18nFactory;

    @Autowired
    public PhoneNumberValidator(final I18nHelper.BeanFactory i18nFactory)
    {
        this.i18nFactory = i18nFactory;
    }

    /**
     * This method is invoked whenever a rule that contains a call number then action is saved, or loaded. If
     * a FAILED result is returned, the error message contained in the result will be displayed to the user, and
     * any save operation will be blocked.
     */
    @Override
    public ValidationResult validate(final ThenActionValidationParam thenActionValidationParam)
    {
        final Option<String> configuredPhoneNumber =
                thenActionValidationParam.getConfiguration().getData().getValue(PHONE_NUMBER_KEY);

        final ApplicationUser userToValidateWith = thenActionValidationParam.getUserToValidateWith();

        // For tutorial purposes, we just check the phone number is not blank
        if (configuredPhoneNumber.isEmpty() || isBlank(configuredPhoneNumber.get()))
        {
            return createResultWithFieldError(
                    userToValidateWith,
                    "call.number.missing");
        }

        return PASSED();
    }

    private ValidationResult createResultWithFieldError(@Nonnull ApplicationUser user, @Nonnull String errorI18nKey)
    {
        final I18nHelper i18nHelper = i18nFactory.getInstance(user);

        Map<String, List<String>> errorList = newHashMap();
        errorList.put(PHONE_NUMBER_KEY, newArrayList(i18nHelper.getText(errorI18nKey)));

        return ValidationResult.FAILED(errorList);
    }
}
