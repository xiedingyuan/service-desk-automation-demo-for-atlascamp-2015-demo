package com.atlassian.plugins.atlascamp.sdautomation;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.Issue;
import com.atlassian.plugins.atlascamp.sdautomation.phonecaller.InternationalPhoneNumber;
import com.atlassian.plugins.atlascamp.sdautomation.phonecaller.PhoneCallTranscript;
import com.atlassian.plugins.atlascamp.sdautomation.phonecaller.PhoneCaller;
import com.atlassian.pocketknife.api.commons.error.AnError;
import com.atlassian.servicedesk.plugins.automation.api.execution.error.ThenActionError;
import com.atlassian.servicedesk.plugins.automation.api.execution.error.ThenActionErrorHelper;
import com.atlassian.servicedesk.plugins.automation.api.execution.message.RuleMessage;
import com.atlassian.servicedesk.plugins.automation.api.execution.message.helper.IssueMessageHelper;
import com.atlassian.servicedesk.plugins.automation.spi.rulethen.ThenAction;
import org.springframework.beans.factory.annotation.Autowired;

import static com.atlassian.fugue.Either.right;
import static com.atlassian.util.concurrent.Assertions.notNull;
import static com.google.common.collect.Lists.newArrayList;

/**
 * Hey, I just met you. And this is crazy. But here's my number. So call me maybe (if an event is detected by the when
 * handler and the if conditions match).
 *
 */
public final class CallMeMaybe implements ThenAction
{
    public static final String PHONE_NUMBER_KEY = "phoneNumber";

    private final PhoneCaller phoneCaller;
    private final IssueMessageHelper issueMessageHelper;
    private final ThenActionErrorHelper thenActionErrorHelper;

    @Autowired
    public CallMeMaybe(final PhoneCaller phoneCaller,
            final IssueMessageHelper issueMessageHelper,
            final ThenActionErrorHelper thenActionErrorHelper)
    {
        notNull("phoneCaller", phoneCaller);
        this.phoneCaller = phoneCaller;
        notNull("issueMessageHelper", issueMessageHelper);
        this.issueMessageHelper = issueMessageHelper;
        notNull("thenActionErrorHelper", thenActionErrorHelper);
        this.thenActionErrorHelper = thenActionErrorHelper;
    }

    @Override
    public Either<ThenActionError, RuleMessage> invoke(final ThenActionParam thenActionParam)
    {
        // Get the phone number we want to call
        final Option<String> phoneNumberOpt = thenActionParam.getConfiguration().getData().getValue(PHONE_NUMBER_KEY);
        if (phoneNumberOpt.isEmpty())
        {
            return thenActionErrorHelper.error("No " + PHONE_NUMBER_KEY + " property in config data");
        }
        final String numberToCall = phoneNumberOpt.get();

        // Get the issue for which we want make a call
        final Either<AnError, Issue> issueEither = issueMessageHelper.getIssue(thenActionParam.getMessage());
        if (issueEither.isLeft())
        {
            // We don't perform any task if we can't get the issue from the rule message
            return thenActionErrorHelper.error(issueEither.left().get());
        }
        final Issue issueToMakeCallFor = issueEither.right().get();

        // Make the call
        phoneCaller.call(
                new InternationalPhoneNumber(numberToCall),
                generateTranscriptForIssue(issueToMakeCallFor));

        return right(thenActionParam.getMessage());
    }

    private PhoneCallTranscript generateTranscriptForIssue(final Issue issue)
    {
        final String reporter = issue.getReporter().getDisplayName();
        final String priority = issue.getPriorityObject().getName();
        final String summary = issue.getSummary();

        final String transcriptLine1 = "Welcome to Service Desk";

        final String transcriptLine2 = new StringBuilder()
                .append(reporter)
                .append(" has raised a ").append(priority).append(" issue with the following summary ")
                .toString();

        final String transcriptLine3 = summary;

        return new PhoneCallTranscript(
                newArrayList(
                        transcriptLine1,
                        transcriptLine2,
                        transcriptLine3
                )
        );
    }
}
