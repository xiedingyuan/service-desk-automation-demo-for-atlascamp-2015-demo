package com.atlassian.plugins.atlascamp.sdautomation.phonecaller;

import org.apache.commons.lang3.StringUtils;

import static com.atlassian.plugin.util.Assertions.isTrue;
import static com.atlassian.util.concurrent.Assertions.notNull;
import static org.apache.commons.lang3.StringUtils.removeStart;

/**
 * Represents a phone number, including the international dialling code.
 *
 */
public final class InternationalPhoneNumber
{
    private final String numberWithIntlCode;

    public InternationalPhoneNumber(final String numberAsString)
    {
        notNull("numberAsString", numberAsString);
        checkForLeadingPlus(numberAsString);
        checkNumber(removeLeadingPlus(numberAsString));

        numberWithIntlCode = numberAsString;
    }

    private void checkForLeadingPlus(final String numberAsString)
    {
        isTrue("'" + numberAsString + "' does not have an international dialling code",
                numberAsString.trim().startsWith("+"));
    }

    private String removeLeadingPlus(final String toRemoveFrom)
    {
        return removeLeading("+", toRemoveFrom);
    }

    private String removeLeading(final String toRemove, final String toRemoveFrom)
    {
        return removeStart(toRemoveFrom.trim(), toRemove);
    }

    private void checkNumber(String toCheck)
    {
        isTrue("'" + toCheck + "' is not a valid number",
                StringUtils.isNumeric(toCheck));
    }

    public String getFullNumber()
    {
        return numberWithIntlCode;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final InternationalPhoneNumber that = (InternationalPhoneNumber) o;

        if (numberWithIntlCode != null ? !numberWithIntlCode.equals(that.numberWithIntlCode) : that.numberWithIntlCode != null)
            return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        return numberWithIntlCode != null ? numberWithIntlCode.hashCode() : 0;
    }

    @Override
    public String toString()
    {
        return getFullNumber();
    }
}
