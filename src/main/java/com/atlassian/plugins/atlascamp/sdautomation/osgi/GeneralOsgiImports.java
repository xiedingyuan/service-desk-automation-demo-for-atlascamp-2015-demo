package com.atlassian.plugins.atlascamp.sdautomation.osgi;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

/**
 * This class is used to replace <component-import /> declarations in the atlassian-plugin.xml.
 * This class will be scanned by the atlassian spring scanner at compile time.
 * There is no situations where you ever need to create this class, it's here purely so that all the component-imports are in the one place and not scattered throughout the code.
 */
@SuppressWarnings("UnusedDeclaration")
@Scanned
public class GeneralOsgiImports
{
    @ComponentImport com.atlassian.jira.util.I18nHelper i18nHelper;
    @ComponentImport com.atlassian.jira.util.I18nHelper.BeanFactory i18nBeanFactory;
    @ComponentImport com.atlassian.servicedesk.plugins.automation.api.execution.message.helper.IssueMessageHelper issueMessageHelper;
    @ComponentImport com.atlassian.servicedesk.plugins.automation.api.execution.error.ThenActionErrorHelper thenActionErrorHelper;

    private GeneralOsgiImports()
    {
        throw new Error("This class should not be instantiated");
    }
}