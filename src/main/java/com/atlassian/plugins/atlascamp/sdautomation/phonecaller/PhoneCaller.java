package com.atlassian.plugins.atlascamp.sdautomation.phonecaller;

/**
 * Makes a phone call to a given phone number.
 *
 */
public interface PhoneCaller
{
    void call(final InternationalPhoneNumber recipient, final PhoneCallTranscript callContents);
}
