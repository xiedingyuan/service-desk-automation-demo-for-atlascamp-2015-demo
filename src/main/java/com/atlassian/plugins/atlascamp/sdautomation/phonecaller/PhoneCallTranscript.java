package com.atlassian.plugins.atlascamp.sdautomation.phonecaller;

import com.google.common.collect.ImmutableList;

import java.util.List;

import static com.atlassian.util.concurrent.Assertions.isTrue;
import static com.atlassian.util.concurrent.Assertions.notNull;

public final class PhoneCallTranscript
{
    private static final int MAX_LINES = 5;

    final List<String> lines;

    public PhoneCallTranscript(final List<String> lines)
    {
        notNull("lines", lines);
        isTrue("Cannot have a transcript with no lines", !lines.isEmpty());
        isTrue("Cannot have a transcript with more than " + MAX_LINES + " lines", lines.size() < MAX_LINES);

        this.lines = ImmutableList.copyOf(lines);
    }

    public List<String> getLines()
    {
        return lines;
    }
}
