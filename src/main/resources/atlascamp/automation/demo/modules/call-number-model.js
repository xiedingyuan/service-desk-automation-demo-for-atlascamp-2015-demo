define("atlascamp/automation/demo/modules/call-number-model", [
    "servicedesk/backbone-brace"
], function (
        Brace
) {

    return Brace.Model.extend({
        namedAttributes: {
            phoneNumber: String
        },
        defaults: {
            phoneNumber: ""
        }
    });
});