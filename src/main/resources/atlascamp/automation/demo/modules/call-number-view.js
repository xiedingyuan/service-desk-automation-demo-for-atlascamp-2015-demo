define("atlascamp/automation/demo/modules/call-number-view", [
    "servicedesk/jQuery",
    "servicedesk/underscore",
    "servicedesk/backbone-brace",
    "servicedesk/util/tracking",
    "servicedesk/shared/mixin/form/form-mixin"
], function (
        $,
        _,
        Brace,
        tracker,
        FormMixin
) {
    return Brace.View.extend({
        template: ServiceDesk.Automation.Demo.CallNumber.drawCallNumberForm,
        mixins: [FormMixin],

        dispose: function() {
            this.undelegateEvents();
            this.stopListening();
        },

        render: function() {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }
    });
});