define("atlascamp/automation/demo/modules/call-number-form", [
    "servicedesk/jQuery",
    "servicedesk/underscore",
    "atlascamp/automation/demo/modules/call-number-model",
    "atlascamp/automation/demo/modules/call-number-view"
], function (
        $,
        _,
        CallNumberModel,
        CallNumberView
) {

    var callNumberView = function(controller) {
        var template = ServiceDesk.Automation.Demo.CallNumber.callNumberContainer;
        var $el = $(controller.el);

        function onError(errors) {
            $el.find('.error').remove();
            _applyFieldErrors(errors.fieldErrors);
            _applyGlobalErrors(errors.globalErrors);
        }

        function onDestroy() {
            controller.off('destroy');
            controller.off('error');
        }

        function _applyFieldErrors(errors) {
            // If errors is an array
            _.each(errors, controller.renderFieldError)
        }

        function _applyGlobalErrors(errors) {
            for (var i = 0; i < errors.length; i++) {
                var thisError = errors[i];
                controller.renderGlobalError(thisError)
            }
        }

        controller.on('destroy', onDestroy.bind(this));
        controller.on('error', onError.bind(this));

        return {
            render: function(config, errors) {
                var phoneNumber = config && config.phoneNumber ? config.phoneNumber : "";

                // Render the template
                $el.html(template());

                this.callNumberView = new CallNumberView({
                    model: new CallNumberModel({
                        phoneNumber: phoneNumber
                    }),
                    el: $el.find(".automation-servicedesk-call-number-container")
                }).render();

                if (errors) {
                    if (errors.fieldErrors) {
                        _applyFieldErrors(errors.fieldErrors);
                    }

                    if (errors.globalErrors) {
                        _applyGlobalErrors(errors.globalErrors);
                    }
                }

                return this;
            },

            serialize: function () {
                return {
                    phoneNumber: $el.find('input').val()
                }
            },

            validate: function (deferred) {
                $el.find('.error').remove();
                var hasError = false;
                var phoneNumberField = $el.find('input');
                var fieldErrors = {};

                if (!phoneNumberField.val()) {
                    fieldErrors[phoneNumberField.attr('name')] = AJS.I18n.getText('call.number.missing');
                    hasError = true;
                }

                if (hasError) {
                    _applyFieldErrors(fieldErrors);
                    deferred.reject();
                }
                else {
                    deferred.resolve();
                }
            },

            dispose: function() {
                if (this.callNumberView) {
                    this.callNumberView.dispose && this.callNumberView.dispose();
                }
            }
        }
    };

    return function(controller) {
        return callNumberView(controller);
    };
});